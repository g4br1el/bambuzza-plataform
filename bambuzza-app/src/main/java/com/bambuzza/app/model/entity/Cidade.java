package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_cidade database table.
 * 
 */
@Entity
@Table(name="tb_cidade")
@NamedQuery(name="Cidade.findAll", query="SELECT c FROM Cidade c")
public class Cidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_CIDADE_IDCIDADE_GENERATOR", sequenceName="ID_CIDADE_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_CIDADE_IDCIDADE_GENERATOR")
	@Column(name="id_cidade")
	private Integer idCidade;

	@Column(name="nome_cidade")
	private String nomeCidade;

	//bi-directional many-to-one association to Endereco
	@OneToMany(mappedBy="tbCidade")
	private List<Endereco> tbEnderecos;

	public Cidade() {
	}

	public Integer getIdCidade() {
		return this.idCidade;
	}

	public void setIdCidade(Integer idCidade) {
		this.idCidade = idCidade;
	}

	public String getNomeCidade() {
		return this.nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}

	public List<Endereco> getTbEnderecos() {
		return this.tbEnderecos;
	}

	public void setTbEnderecos(List<Endereco> tbEnderecos) {
		this.tbEnderecos = tbEnderecos;
	}

	public Endereco addTbEndereco(Endereco tbEndereco) {
		getTbEnderecos().add(tbEndereco);
		tbEndereco.setTbCidade(this);

		return tbEndereco;
	}

	public Endereco removeTbEndereco(Endereco tbEndereco) {
		getTbEnderecos().remove(tbEndereco);
		tbEndereco.setTbCidade(null);

		return tbEndereco;
	}

}