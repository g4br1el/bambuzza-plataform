package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_tipo_usuario database table.
 * 
 */
@Entity
@Table(name="tb_tipo_usuario")
@NamedQuery(name="TipoUsuario.findAll", query="SELECT t FROM TipoUsuario t")
public class TipoUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_TIPO_USUARIO_IDTIPOUSUARIO_GENERATOR", sequenceName="ID_TIPO_USUARIO_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_TIPO_USUARIO_IDTIPOUSUARIO_GENERATOR")
	@Column(name="id_tipo_usuario")
	private Integer idTipoUsuario;

	private String nome;

	//bi-directional many-to-one association to Pergunta
	@OneToMany(mappedBy="tbTipoUsuario")
	private List<Pergunta> tbPerguntas;

	//bi-directional many-to-one association to Questionario
	@OneToMany(mappedBy="tbTipoUsuario")
	private List<Questionario> tbQuestionarios;

	//bi-directional many-to-many association to TipoTelefone
	@ManyToMany
	@JoinTable(
		name="tb_telefone_tipo_usuario"
		, joinColumns={
			@JoinColumn(name="id_tipo_usuario")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_tipo_telefone")
			}
		)
	private List<TipoTelefone> tbTipoTelefones;

	//bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy="tbTipoUsuario")
	private List<Usuario> tbUsuarios;

	public TipoUsuario() {
	}

	public Integer getIdTipoUsuario() {
		return this.idTipoUsuario;
	}

	public void setIdTipoUsuario(Integer idTipoUsuario) {
		this.idTipoUsuario = idTipoUsuario;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Pergunta> getTbPerguntas() {
		return this.tbPerguntas;
	}

	public void setTbPerguntas(List<Pergunta> tbPerguntas) {
		this.tbPerguntas = tbPerguntas;
	}

	public Pergunta addTbPergunta(Pergunta tbPergunta) {
		getTbPerguntas().add(tbPergunta);
		tbPergunta.setTbTipoUsuario(this);

		return tbPergunta;
	}

	public Pergunta removeTbPergunta(Pergunta tbPergunta) {
		getTbPerguntas().remove(tbPergunta);
		tbPergunta.setTbTipoUsuario(null);

		return tbPergunta;
	}

	public List<Questionario> getTbQuestionarios() {
		return this.tbQuestionarios;
	}

	public void setTbQuestionarios(List<Questionario> tbQuestionarios) {
		this.tbQuestionarios = tbQuestionarios;
	}

	public Questionario addTbQuestionario(Questionario tbQuestionario) {
		getTbQuestionarios().add(tbQuestionario);
		tbQuestionario.setTbTipoUsuario(this);

		return tbQuestionario;
	}

	public Questionario removeTbQuestionario(Questionario tbQuestionario) {
		getTbQuestionarios().remove(tbQuestionario);
		tbQuestionario.setTbTipoUsuario(null);

		return tbQuestionario;
	}

	public List<TipoTelefone> getTbTipoTelefones() {
		return this.tbTipoTelefones;
	}

	public void setTbTipoTelefones(List<TipoTelefone> tbTipoTelefones) {
		this.tbTipoTelefones = tbTipoTelefones;
	}

	public List<Usuario> getTbUsuarios() {
		return this.tbUsuarios;
	}

	public void setTbUsuarios(List<Usuario> tbUsuarios) {
		this.tbUsuarios = tbUsuarios;
	}

	public Usuario addTbUsuario(Usuario tbUsuario) {
		getTbUsuarios().add(tbUsuario);
		tbUsuario.setTbTipoUsuario(this);

		return tbUsuario;
	}

	public Usuario removeTbUsuario(Usuario tbUsuario) {
		getTbUsuarios().remove(tbUsuario);
		tbUsuario.setTbTipoUsuario(null);

		return tbUsuario;
	}

}