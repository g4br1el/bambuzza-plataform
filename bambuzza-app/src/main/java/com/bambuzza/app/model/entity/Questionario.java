package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the tb_questionario database table.
 * 
 */
@Entity
@Table(name="tb_questionario")
@NamedQuery(name="Questionario.findAll", query="SELECT q FROM Questionario q")
public class Questionario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_QUESTIONARIO_IDQUESTIONARIO_GENERATOR", sequenceName="ID_QUESTIONARIO_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_QUESTIONARIO_IDQUESTIONARIO_GENERATOR")
	@Column(name="id_questionario")
	private Integer idQuestionario;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_enviado_selecao")
	private Date dtEnviadoSelecao;

	@Column(name="dt_ultima_modificacao")
	private Timestamp dtUltimaModificacao;

	@Column(name="enviado_selecao")
	private Boolean enviadoSelecao;

	@Column(name="id_usuario_ultima_modf")
	private Integer idUsuarioUltimaModf;

	//bi-directional many-to-one association to TipoUsuario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_usuario")
	private TipoUsuario tbTipoUsuario;

	//bi-directional many-to-many association to Pergunta
	@ManyToMany
	@JoinTable(
		name="tb_questionario_pergunta"
		, joinColumns={
			@JoinColumn(name="id_questionario")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_pergunta")
			}
		)
	private List<Pergunta> tbPerguntas;

	//bi-directional many-to-one association to Resposta
	@OneToMany(mappedBy="tbQuestionario")
	private List<Resposta> tbRespostas;

	//bi-directional one-to-one association to Empresa
	@OneToOne(mappedBy="tbQuestionario", fetch=FetchType.LAZY)
	private Empresa tbEmpresa;

	//bi-directional one-to-one association to Usuario
	@OneToOne(mappedBy="tbQuestionario", fetch=FetchType.LAZY)
	private Usuario tbUsuario;

	public Questionario() {
	}

	public Integer getIdQuestionario() {
		return this.idQuestionario;
	}

	public void setIdQuestionario(Integer idQuestionario) {
		this.idQuestionario = idQuestionario;
	}

	public Date getDtEnviadoSelecao() {
		return this.dtEnviadoSelecao;
	}

	public void setDtEnviadoSelecao(Date dtEnviadoSelecao) {
		this.dtEnviadoSelecao = dtEnviadoSelecao;
	}

	public Timestamp getDtUltimaModificacao() {
		return this.dtUltimaModificacao;
	}

	public void setDtUltimaModificacao(Timestamp dtUltimaModificacao) {
		this.dtUltimaModificacao = dtUltimaModificacao;
	}

	public Boolean getEnviadoSelecao() {
		return this.enviadoSelecao;
	}

	public void setEnviadoSelecao(Boolean enviadoSelecao) {
		this.enviadoSelecao = enviadoSelecao;
	}

	public Integer getIdUsuarioUltimaModf() {
		return this.idUsuarioUltimaModf;
	}

	public void setIdUsuarioUltimaModf(Integer idUsuarioUltimaModf) {
		this.idUsuarioUltimaModf = idUsuarioUltimaModf;
	}

	public TipoUsuario getTbTipoUsuario() {
		return this.tbTipoUsuario;
	}

	public void setTbTipoUsuario(TipoUsuario tbTipoUsuario) {
		this.tbTipoUsuario = tbTipoUsuario;
	}

	public List<Pergunta> getTbPerguntas() {
		return this.tbPerguntas;
	}

	public void setTbPerguntas(List<Pergunta> tbPerguntas) {
		this.tbPerguntas = tbPerguntas;
	}

	public List<Resposta> getTbRespostas() {
		return this.tbRespostas;
	}

	public void setTbRespostas(List<Resposta> tbRespostas) {
		this.tbRespostas = tbRespostas;
	}

	public Resposta addTbResposta(Resposta tbResposta) {
		getTbRespostas().add(tbResposta);
		tbResposta.setTbQuestionario(this);

		return tbResposta;
	}

	public Resposta removeTbResposta(Resposta tbResposta) {
		getTbRespostas().remove(tbResposta);
		tbResposta.setTbQuestionario(null);

		return tbResposta;
	}

	public Empresa getTbEmpresa() {
		return this.tbEmpresa;
	}

	public void setTbEmpresa(Empresa tbEmpresa) {
		this.tbEmpresa = tbEmpresa;
	}

	public Usuario getTbUsuario() {
		return this.tbUsuario;
	}

	public void setTbUsuario(Usuario tbUsuario) {
		this.tbUsuario = tbUsuario;
	}

}