package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_pais database table.
 * 
 */
@Entity
@Table(name="tb_pais")
@NamedQuery(name="Pai.findAll", query="SELECT p FROM Pai p")
public class Pai implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_PAIS_IDPAIS_GENERATOR", sequenceName="ID_PAIS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_PAIS_IDPAIS_GENERATOR")
	@Column(name="id_pais")
	private Integer idPais;

	@Column(name="nome_pais")
	private String nomePais;

	//bi-directional many-to-one association to Endereco
	@OneToMany(mappedBy="tbPai")
	private List<Endereco> tbEnderecos;

	public Pai() {
	}

	public Integer getIdPais() {
		return this.idPais;
	}

	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}

	public String getNomePais() {
		return this.nomePais;
	}

	public void setNomePais(String nomePais) {
		this.nomePais = nomePais;
	}

	public List<Endereco> getTbEnderecos() {
		return this.tbEnderecos;
	}

	public void setTbEnderecos(List<Endereco> tbEnderecos) {
		this.tbEnderecos = tbEnderecos;
	}

	public Endereco addTbEndereco(Endereco tbEndereco) {
		getTbEnderecos().add(tbEndereco);
		tbEndereco.setTbPai(this);

		return tbEndereco;
	}

	public Endereco removeTbEndereco(Endereco tbEndereco) {
		getTbEnderecos().remove(tbEndereco);
		tbEndereco.setTbPai(null);

		return tbEndereco;
	}

}