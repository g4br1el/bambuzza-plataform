package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the tb_empresa database table.
 * 
 */
@Entity
@Table(name="tb_empresa")
@NamedQuery(name="Empresa.findAll", query="SELECT e FROM Empresa e")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_EMPRESA_IDEMPRESA_GENERATOR", sequenceName="ID_EMPRESA_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_EMPRESA_IDEMPRESA_GENERATOR")
	@Column(name="id_empresa")
	private Integer idEmpresa;

	private Boolean aprovada;

	private String cnpj;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_aprovacao")
	private Date dtAprovacao;

	@Column(name="img_descricao")
	private String imgDescricao;

	@Column(name="img_logo")
	private String imgLogo;

	@Column(name="nome_empresa")
	private String nomeEmpresa;

	private String pitch;

	@Column(name="plano_negocio")
	private String planoNegocio;

	//bi-directional many-to-one association to Telefone
	@OneToMany(mappedBy="tbEmpresa")
	private List<Telefone> tbTelefones;

	//bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy="tbEmpresa")
	private List<Usuario> tbUsuarios;

	//bi-directional one-to-one association to Endereco
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_endereco")
	private Endereco tbEndereco;

	//bi-directional one-to-one association to Questionario
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_questionario")
	private Questionario tbQuestionario;

	public Empresa() {
	}

	public Integer getIdEmpresa() {
		return this.idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Boolean getAprovada() {
		return this.aprovada;
	}

	public void setAprovada(Boolean aprovada) {
		this.aprovada = aprovada;
	}

	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Date getDtAprovacao() {
		return this.dtAprovacao;
	}

	public void setDtAprovacao(Date dtAprovacao) {
		this.dtAprovacao = dtAprovacao;
	}

	public String getImgDescricao() {
		return this.imgDescricao;
	}

	public void setImgDescricao(String imgDescricao) {
		this.imgDescricao = imgDescricao;
	}

	public String getImgLogo() {
		return this.imgLogo;
	}

	public void setImgLogo(String imgLogo) {
		this.imgLogo = imgLogo;
	}

	public String getNomeEmpresa() {
		return this.nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getPitch() {
		return this.pitch;
	}

	public void setPitch(String pitch) {
		this.pitch = pitch;
	}

	public String getPlanoNegocio() {
		return this.planoNegocio;
	}

	public void setPlanoNegocio(String planoNegocio) {
		this.planoNegocio = planoNegocio;
	}

	public List<Telefone> getTbTelefones() {
		return this.tbTelefones;
	}

	public void setTbTelefones(List<Telefone> tbTelefones) {
		this.tbTelefones = tbTelefones;
	}

	public Telefone addTbTelefone(Telefone tbTelefone) {
		getTbTelefones().add(tbTelefone);
		tbTelefone.setTbEmpresa(this);

		return tbTelefone;
	}

	public Telefone removeTbTelefone(Telefone tbTelefone) {
		getTbTelefones().remove(tbTelefone);
		tbTelefone.setTbEmpresa(null);

		return tbTelefone;
	}

	public List<Usuario> getTbUsuarios() {
		return this.tbUsuarios;
	}

	public void setTbUsuarios(List<Usuario> tbUsuarios) {
		this.tbUsuarios = tbUsuarios;
	}

	public Usuario addTbUsuario(Usuario tbUsuario) {
		getTbUsuarios().add(tbUsuario);
		tbUsuario.setTbEmpresa(this);

		return tbUsuario;
	}

	public Usuario removeTbUsuario(Usuario tbUsuario) {
		getTbUsuarios().remove(tbUsuario);
		tbUsuario.setTbEmpresa(null);

		return tbUsuario;
	}

	public Endereco getTbEndereco() {
		return this.tbEndereco;
	}

	public void setTbEndereco(Endereco tbEndereco) {
		this.tbEndereco = tbEndereco;
	}

	public Questionario getTbQuestionario() {
		return this.tbQuestionario;
	}

	public void setTbQuestionario(Questionario tbQuestionario) {
		this.tbQuestionario = tbQuestionario;
	}

}