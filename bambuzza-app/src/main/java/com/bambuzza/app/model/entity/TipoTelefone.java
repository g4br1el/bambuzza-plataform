package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_tipo_telefone database table.
 * 
 */
@Entity
@Table(name="tb_tipo_telefone")
@NamedQuery(name="TipoTelefone.findAll", query="SELECT t FROM TipoTelefone t")
public class TipoTelefone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_TIPO_TELEFONE_IDTIPOTELEFONE_GENERATOR", sequenceName="ID_TIPO_TELEFONE_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_TIPO_TELEFONE_IDTIPOTELEFONE_GENERATOR")
	@Column(name="id_tipo_telefone")
	private Integer idTipoTelefone;

	@Column(name="nome_tipo_telefone")
	private String nomeTipoTelefone;

	//bi-directional many-to-one association to Telefone
	@OneToMany(mappedBy="tbTipoTelefone")
	private List<Telefone> tbTelefones;

	//bi-directional many-to-many association to TipoUsuario
	@ManyToMany(mappedBy="tbTipoTelefones")
	private List<TipoUsuario> tbTipoUsuarios;

	public TipoTelefone() {
	}

	public Integer getIdTipoTelefone() {
		return this.idTipoTelefone;
	}

	public void setIdTipoTelefone(Integer idTipoTelefone) {
		this.idTipoTelefone = idTipoTelefone;
	}

	public String getNomeTipoTelefone() {
		return this.nomeTipoTelefone;
	}

	public void setNomeTipoTelefone(String nomeTipoTelefone) {
		this.nomeTipoTelefone = nomeTipoTelefone;
	}

	public List<Telefone> getTbTelefones() {
		return this.tbTelefones;
	}

	public void setTbTelefones(List<Telefone> tbTelefones) {
		this.tbTelefones = tbTelefones;
	}

	public Telefone addTbTelefone(Telefone tbTelefone) {
		getTbTelefones().add(tbTelefone);
		tbTelefone.setTbTipoTelefone(this);

		return tbTelefone;
	}

	public Telefone removeTbTelefone(Telefone tbTelefone) {
		getTbTelefones().remove(tbTelefone);
		tbTelefone.setTbTipoTelefone(null);

		return tbTelefone;
	}

	public List<TipoUsuario> getTbTipoUsuarios() {
		return this.tbTipoUsuarios;
	}

	public void setTbTipoUsuarios(List<TipoUsuario> tbTipoUsuarios) {
		this.tbTipoUsuarios = tbTipoUsuarios;
	}

}