package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_pergunta database table.
 * 
 */
@Entity
@Table(name="tb_pergunta")
@NamedQuery(name="Pergunta.findAll", query="SELECT p FROM Pergunta p")
public class Pergunta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_PERGUNTA_IDPERGUNTA_GENERATOR", sequenceName="ID_PERGUNTA_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_PERGUNTA_IDPERGUNTA_GENERATOR")
	@Column(name="id_pergunta")
	private Integer idPergunta;

	private Boolean ativa;

	@Column(name="exemplo_resposta")
	private String exemploResposta;

	@Column(name="texto_pergunta")
	private String textoPergunta;

	//bi-directional many-to-one association to OpcaoResposta
	@OneToMany(mappedBy="tbPergunta")
	private List<OpcaoResposta> tbOpcaoRespostas;

	//bi-directional many-to-one association to TipoResposta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_resposta")
	private TipoResposta tbTipoResposta;

	//bi-directional many-to-one association to TipoUsuario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_usuario")
	private TipoUsuario tbTipoUsuario;

	//bi-directional many-to-many association to Questionario
	@ManyToMany(mappedBy="tbPerguntas")
	private List<Questionario> tbQuestionarios;

	//bi-directional many-to-one association to Resposta
	@OneToMany(mappedBy="tbPergunta")
	private List<Resposta> tbRespostas;

	public Pergunta() {
	}

	public Integer getIdPergunta() {
		return this.idPergunta;
	}

	public void setIdPergunta(Integer idPergunta) {
		this.idPergunta = idPergunta;
	}

	public Boolean getAtiva() {
		return this.ativa;
	}

	public void setAtiva(Boolean ativa) {
		this.ativa = ativa;
	}

	public String getExemploResposta() {
		return this.exemploResposta;
	}

	public void setExemploResposta(String exemploResposta) {
		this.exemploResposta = exemploResposta;
	}

	public String getTextoPergunta() {
		return this.textoPergunta;
	}

	public void setTextoPergunta(String textoPergunta) {
		this.textoPergunta = textoPergunta;
	}

	public List<OpcaoResposta> getTbOpcaoRespostas() {
		return this.tbOpcaoRespostas;
	}

	public void setTbOpcaoRespostas(List<OpcaoResposta> tbOpcaoRespostas) {
		this.tbOpcaoRespostas = tbOpcaoRespostas;
	}

	public OpcaoResposta addTbOpcaoResposta(OpcaoResposta tbOpcaoResposta) {
		getTbOpcaoRespostas().add(tbOpcaoResposta);
		tbOpcaoResposta.setTbPergunta(this);

		return tbOpcaoResposta;
	}

	public OpcaoResposta removeTbOpcaoResposta(OpcaoResposta tbOpcaoResposta) {
		getTbOpcaoRespostas().remove(tbOpcaoResposta);
		tbOpcaoResposta.setTbPergunta(null);

		return tbOpcaoResposta;
	}

	public TipoResposta getTbTipoResposta() {
		return this.tbTipoResposta;
	}

	public void setTbTipoResposta(TipoResposta tbTipoResposta) {
		this.tbTipoResposta = tbTipoResposta;
	}

	public TipoUsuario getTbTipoUsuario() {
		return this.tbTipoUsuario;
	}

	public void setTbTipoUsuario(TipoUsuario tbTipoUsuario) {
		this.tbTipoUsuario = tbTipoUsuario;
	}

	public List<Questionario> getTbQuestionarios() {
		return this.tbQuestionarios;
	}

	public void setTbQuestionarios(List<Questionario> tbQuestionarios) {
		this.tbQuestionarios = tbQuestionarios;
	}

	public List<Resposta> getTbRespostas() {
		return this.tbRespostas;
	}

	public void setTbRespostas(List<Resposta> tbRespostas) {
		this.tbRespostas = tbRespostas;
	}

	public Resposta addTbResposta(Resposta tbResposta) {
		getTbRespostas().add(tbResposta);
		tbResposta.setTbPergunta(this);

		return tbResposta;
	}

	public Resposta removeTbResposta(Resposta tbResposta) {
		getTbRespostas().remove(tbResposta);
		tbResposta.setTbPergunta(null);

		return tbResposta;
	}

}