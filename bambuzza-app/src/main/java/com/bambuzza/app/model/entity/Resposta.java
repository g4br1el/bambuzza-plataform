package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_resposta database table.
 * 
 */
@Entity
@Table(name="tb_resposta")
@NamedQuery(name="Resposta.findAll", query="SELECT r FROM Resposta r")
public class Resposta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_RESPOSTA_IDRESPOSTA_GENERATOR", sequenceName="ID_RESPOSTA_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_RESPOSTA_IDRESPOSTA_GENERATOR")
	@Column(name="id_resposta")
	private Integer idResposta;

	@Column(name="id_resposta_multipla_escolha")
	private Integer idRespostaMultiplaEscolha;

	@Column(name="resposta_boleana")
	private Boolean respostaBoleana;

	@Column(name="resposta_numero")
	private Integer respostaNumero;

	@Column(name="resposta_texto_curto")
	private String respostaTextoCurto;

	@Column(name="resposta_texto_longo")
	private String respostaTextoLongo;

	//bi-directional many-to-many association to OpcaoResposta
	@ManyToMany
	@JoinTable(
		name="tb_opcao_resposta_pergunta"
		, joinColumns={
			@JoinColumn(name="id_resposta")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_opcao_resposta")
			}
		)
	private List<OpcaoResposta> tbOpcaoRespostas;

	//bi-directional many-to-one association to Pergunta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_pergunta")
	private Pergunta tbPergunta;

	//bi-directional many-to-one association to Questionario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_questionario_empresa")
	private Questionario tbQuestionario;

	public Resposta() {
	}

	public Integer getIdResposta() {
		return this.idResposta;
	}

	public void setIdResposta(Integer idResposta) {
		this.idResposta = idResposta;
	}

	public Integer getIdRespostaMultiplaEscolha() {
		return this.idRespostaMultiplaEscolha;
	}

	public void setIdRespostaMultiplaEscolha(Integer idRespostaMultiplaEscolha) {
		this.idRespostaMultiplaEscolha = idRespostaMultiplaEscolha;
	}

	public Boolean getRespostaBoleana() {
		return this.respostaBoleana;
	}

	public void setRespostaBoleana(Boolean respostaBoleana) {
		this.respostaBoleana = respostaBoleana;
	}

	public Integer getRespostaNumero() {
		return this.respostaNumero;
	}

	public void setRespostaNumero(Integer respostaNumero) {
		this.respostaNumero = respostaNumero;
	}

	public String getRespostaTextoCurto() {
		return this.respostaTextoCurto;
	}

	public void setRespostaTextoCurto(String respostaTextoCurto) {
		this.respostaTextoCurto = respostaTextoCurto;
	}

	public String getRespostaTextoLongo() {
		return this.respostaTextoLongo;
	}

	public void setRespostaTextoLongo(String respostaTextoLongo) {
		this.respostaTextoLongo = respostaTextoLongo;
	}

	public List<OpcaoResposta> getTbOpcaoRespostas() {
		return this.tbOpcaoRespostas;
	}

	public void setTbOpcaoRespostas(List<OpcaoResposta> tbOpcaoRespostas) {
		this.tbOpcaoRespostas = tbOpcaoRespostas;
	}

	public Pergunta getTbPergunta() {
		return this.tbPergunta;
	}

	public void setTbPergunta(Pergunta tbPergunta) {
		this.tbPergunta = tbPergunta;
	}

	public Questionario getTbQuestionario() {
		return this.tbQuestionario;
	}

	public void setTbQuestionario(Questionario tbQuestionario) {
		this.tbQuestionario = tbQuestionario;
	}

}