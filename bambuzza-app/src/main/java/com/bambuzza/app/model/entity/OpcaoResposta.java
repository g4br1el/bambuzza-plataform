package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_opcao_resposta database table.
 * 
 */
@Entity
@Table(name="tb_opcao_resposta")
@NamedQuery(name="OpcaoResposta.findAll", query="SELECT o FROM OpcaoResposta o")
public class OpcaoResposta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_OPCAO_RESPOSTA_IDOPCAORESPOSTA_GENERATOR", sequenceName="ID_OPCAO_RESPOSTA_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_OPCAO_RESPOSTA_IDOPCAORESPOSTA_GENERATOR")
	@Column(name="id_opcao_resposta")
	private Integer idOpcaoResposta;

	@Column(name="opcao_ativa")
	private Boolean opcaoAtiva;

	@Column(name="opcao_nome")
	private String opcaoNome;

	//bi-directional many-to-one association to Pergunta
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_pergunta")
	private Pergunta tbPergunta;

	//bi-directional many-to-many association to Resposta
	@ManyToMany(mappedBy="tbOpcaoRespostas")
	private List<Resposta> tbRespostas;

	public OpcaoResposta() {
	}

	public Integer getIdOpcaoResposta() {
		return this.idOpcaoResposta;
	}

	public void setIdOpcaoResposta(Integer idOpcaoResposta) {
		this.idOpcaoResposta = idOpcaoResposta;
	}

	public Boolean getOpcaoAtiva() {
		return this.opcaoAtiva;
	}

	public void setOpcaoAtiva(Boolean opcaoAtiva) {
		this.opcaoAtiva = opcaoAtiva;
	}

	public String getOpcaoNome() {
		return this.opcaoNome;
	}

	public void setOpcaoNome(String opcaoNome) {
		this.opcaoNome = opcaoNome;
	}

	public Pergunta getTbPergunta() {
		return this.tbPergunta;
	}

	public void setTbPergunta(Pergunta tbPergunta) {
		this.tbPergunta = tbPergunta;
	}

	public List<Resposta> getTbRespostas() {
		return this.tbRespostas;
	}

	public void setTbRespostas(List<Resposta> tbRespostas) {
		this.tbRespostas = tbRespostas;
	}

}