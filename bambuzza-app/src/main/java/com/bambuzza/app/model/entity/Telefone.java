package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_telefone database table.
 * 
 */
@Entity
@Table(name="tb_telefone")
@NamedQuery(name="Telefone.findAll", query="SELECT t FROM Telefone t")
public class Telefone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_TELEFONE_IDTELEFONE_GENERATOR", sequenceName="ID_TELEFONE_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_TELEFONE_IDTELEFONE_GENERATOR")
	@Column(name="id_telefone")
	private Integer idTelefone;

	private String numero;

	private String ramal;

	@Column(name="tem_ramal")
	private Boolean temRamal;

	//bi-directional many-to-one association to Empresa
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_empresa")
	private Empresa tbEmpresa;

	//bi-directional many-to-one association to TipoTelefone
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_numero")
	private TipoTelefone tbTipoTelefone;

	//bi-directional many-to-one association to Usuario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario tbUsuario;

	public Telefone() {
	}

	public Integer getIdTelefone() {
		return this.idTelefone;
	}

	public void setIdTelefone(Integer idTelefone) {
		this.idTelefone = idTelefone;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getRamal() {
		return this.ramal;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	public Boolean getTemRamal() {
		return this.temRamal;
	}

	public void setTemRamal(Boolean temRamal) {
		this.temRamal = temRamal;
	}

	public Empresa getTbEmpresa() {
		return this.tbEmpresa;
	}

	public void setTbEmpresa(Empresa tbEmpresa) {
		this.tbEmpresa = tbEmpresa;
	}

	public TipoTelefone getTbTipoTelefone() {
		return this.tbTipoTelefone;
	}

	public void setTbTipoTelefone(TipoTelefone tbTipoTelefone) {
		this.tbTipoTelefone = tbTipoTelefone;
	}

	public Usuario getTbUsuario() {
		return this.tbUsuario;
	}

	public void setTbUsuario(Usuario tbUsuario) {
		this.tbUsuario = tbUsuario;
	}

}