package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tb_tipo_resposta database table.
 * 
 */
@Entity
@Table(name="tb_tipo_resposta")
@NamedQuery(name="TipoResposta.findAll", query="SELECT t FROM TipoResposta t")
public class TipoResposta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_TIPO_RESPOSTA_IDTIPORESPOSTA_GENERATOR", sequenceName="ID_TIPO_RESPOSTA_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_TIPO_RESPOSTA_IDTIPORESPOSTA_GENERATOR")
	@Column(name="id_tipo_resposta")
	private Integer idTipoResposta;

	@Column(name="tipo_nome")
	private String tipoNome;

	//bi-directional many-to-one association to Pergunta
	@OneToMany(mappedBy="tbTipoResposta")
	private List<Pergunta> tbPerguntas;

	public TipoResposta() {
	}

	public Integer getIdTipoResposta() {
		return this.idTipoResposta;
	}

	public void setIdTipoResposta(Integer idTipoResposta) {
		this.idTipoResposta = idTipoResposta;
	}

	public String getTipoNome() {
		return this.tipoNome;
	}

	public void setTipoNome(String tipoNome) {
		this.tipoNome = tipoNome;
	}

	public List<Pergunta> getTbPerguntas() {
		return this.tbPerguntas;
	}

	public void setTbPerguntas(List<Pergunta> tbPerguntas) {
		this.tbPerguntas = tbPerguntas;
	}

	public Pergunta addTbPergunta(Pergunta tbPergunta) {
		getTbPerguntas().add(tbPergunta);
		tbPergunta.setTbTipoResposta(this);

		return tbPergunta;
	}

	public Pergunta removeTbPergunta(Pergunta tbPergunta) {
		getTbPerguntas().remove(tbPergunta);
		tbPergunta.setTbTipoResposta(null);

		return tbPergunta;
	}

}