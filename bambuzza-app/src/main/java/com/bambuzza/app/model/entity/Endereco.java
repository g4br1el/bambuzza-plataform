package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_endereco database table.
 * 
 */
@Entity
@Table(name="tb_endereco")
@NamedQuery(name="Endereco.findAll", query="SELECT e FROM Endereco e")
public class Endereco implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_ENDERECO_IDENDERECO_GENERATOR", sequenceName="ID_ENDERECO_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_ENDERECO_IDENDERECO_GENERATOR")
	@Column(name="id_endereco")
	private Integer idEndereco;

	private String cep;

	private String complemento;

	//bi-directional many-to-one association to Cidade
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_cidade")
	private Cidade tbCidade;

	//bi-directional many-to-one association to Estado
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_estado")
	private Estado tbEstado;

	//bi-directional many-to-one association to Pai
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_pais")
	private Pai tbPai;

	//bi-directional one-to-one association to Empresa
	@OneToOne(mappedBy="tbEndereco", fetch=FetchType.LAZY)
	private Empresa tbEmpresa;

	//bi-directional one-to-one association to Usuario
	@OneToOne(mappedBy="tbEndereco", fetch=FetchType.LAZY)
	private Usuario tbUsuario;

	public Endereco() {
	}

	public Integer getIdEndereco() {
		return this.idEndereco;
	}

	public void setIdEndereco(Integer idEndereco) {
		this.idEndereco = idEndereco;
	}

	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Cidade getTbCidade() {
		return this.tbCidade;
	}

	public void setTbCidade(Cidade tbCidade) {
		this.tbCidade = tbCidade;
	}

	public Estado getTbEstado() {
		return this.tbEstado;
	}

	public void setTbEstado(Estado tbEstado) {
		this.tbEstado = tbEstado;
	}

	public Pai getTbPai() {
		return this.tbPai;
	}

	public void setTbPai(Pai tbPai) {
		this.tbPai = tbPai;
	}

	public Empresa getTbEmpresa() {
		return this.tbEmpresa;
	}

	public void setTbEmpresa(Empresa tbEmpresa) {
		this.tbEmpresa = tbEmpresa;
	}

	public Usuario getTbUsuario() {
		return this.tbUsuario;
	}

	public void setTbUsuario(Usuario tbUsuario) {
		this.tbUsuario = tbUsuario;
	}

}