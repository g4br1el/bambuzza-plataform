package com.bambuzza.app.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the tb_usuario database table.
 * 
 */
@Entity
@Table(name="tb_usuario")
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TB_USUARIO_IDUSUARIO_GENERATOR", sequenceName="ID_USUARIO_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TB_USUARIO_IDUSUARIO_GENERATOR")
	@Column(name="id_usuario")
	private Integer idUsuario;

	private String cargo;

	private String cpf;

	@Column(name="descricao_profissional")
	private String descricaoProfissional;

	@Temporal(TemporalType.DATE)
	@Column(name="dt_cadastro")
	private Date dtCadastro;

	private String email;

	private String foto;

	@Column(name="nome_usuario")
	private String nomeUsuario;

	private String senha;

	private String sexo;

	//bi-directional many-to-one association to Telefone
	@OneToMany(mappedBy="tbUsuario")
	private List<Telefone> tbTelefones;

	//bi-directional many-to-one association to Empresa
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_empresa")
	private Empresa tbEmpresa;

	//bi-directional many-to-one association to TipoUsuario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_tipo_usuario")
	private TipoUsuario tbTipoUsuario;

	//bi-directional one-to-one association to Endereco
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_endereco")
	private Endereco tbEndereco;

	//bi-directional one-to-one association to Questionario
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_questionario")
	private Questionario tbQuestionario;

	public Usuario() {
	}

	public Integer getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getCargo() {
		return this.cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDescricaoProfissional() {
		return this.descricaoProfissional;
	}

	public void setDescricaoProfissional(String descricaoProfissional) {
		this.descricaoProfissional = descricaoProfissional;
	}

	public Date getDtCadastro() {
		return this.dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFoto() {
		return this.foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getNomeUsuario() {
		return this.nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public List<Telefone> getTbTelefones() {
		return this.tbTelefones;
	}

	public void setTbTelefones(List<Telefone> tbTelefones) {
		this.tbTelefones = tbTelefones;
	}

	public Telefone addTbTelefone(Telefone tbTelefone) {
		getTbTelefones().add(tbTelefone);
		tbTelefone.setTbUsuario(this);

		return tbTelefone;
	}

	public Telefone removeTbTelefone(Telefone tbTelefone) {
		getTbTelefones().remove(tbTelefone);
		tbTelefone.setTbUsuario(null);

		return tbTelefone;
	}

	public Empresa getTbEmpresa() {
		return this.tbEmpresa;
	}

	public void setTbEmpresa(Empresa tbEmpresa) {
		this.tbEmpresa = tbEmpresa;
	}

	public TipoUsuario getTbTipoUsuario() {
		return this.tbTipoUsuario;
	}

	public void setTbTipoUsuario(TipoUsuario tbTipoUsuario) {
		this.tbTipoUsuario = tbTipoUsuario;
	}

	public Endereco getTbEndereco() {
		return this.tbEndereco;
	}

	public void setTbEndereco(Endereco tbEndereco) {
		this.tbEndereco = tbEndereco;
	}

	public Questionario getTbQuestionario() {
		return this.tbQuestionario;
	}

	public void setTbQuestionario(Questionario tbQuestionario) {
		this.tbQuestionario = tbQuestionario;
	}

}