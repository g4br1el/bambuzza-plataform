package com.bambuzza.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
    public String getIndexPage() {
        return "home";
    }
	
	@RequestMapping(value = "/home2", method = RequestMethod.GET)
    public String getSecondPage() {
        return "home2";
    }
	
	@RequestMapping(value = "partials/cadastro/pre", method = RequestMethod.GET)
	public String getPreCadsatro() {
		return "partials/cadastro/pre";
	}
	
	@RequestMapping(value = "partials/cadastro/investidor", method = RequestMethod.GET)
	public String getCadastroInvestidorPage() {
		return "partials/cadastro/investidor";
	}
	
	@RequestMapping(value = "partials/cadastro/empresa", method = RequestMethod.GET)
	public String getCadastroEmpresaPage() {
		return "partials/cadastro/empresa";
	}
	  
}
