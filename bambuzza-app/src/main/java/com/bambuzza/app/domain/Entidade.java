package com.bambuzza.app.domain;

import java.io.Serializable;


public interface Entidade extends Serializable{

	public long getId();
	
	public void setId(long id);
}
