var app = angular.module("bambuzza", ['ngRoute']);

app.config(function($routeProvider, $locationProvider){
	$routeProvider.when('/', {
		templateUrl : "home2",
		controller: "HomeController"
	}).
	when('/cadastro', {
		templateUrl: 'partials/cadastro/pre.html',
		controller: 'PreCadastroController'
	}).
	when('/cadastro/investidor', {
		templateUrl: 'partials/cadastro/investidor.html',
		controller: 'CadastroInvestidorController'
	}).
	when('/cadastro/empresa', {
		templateUrl: 'partials/cadastro/empresa.html',
		controller: 'CadastroEmpresaController'
	})
	.otherwise({redirectTo: '/'});
	
	/*$locationProvider.html5Mode(true);*/
});

app.controller("HomeController", function(){
	
});

app.service("UserService", function(){
	var User = {
			nome: null,
			email: null,
			senha: null
	} 
});

app.controller("PreCadastroController", ["$scope", function($scope) {
	$scope.lerContrato = false;
}]);

app.controller("CadastroEmpresaController", ["$scope", function($scope) {
	$scope.socios = [{id: 1}];
	$scope.setores = [
	    {
	    	name: "Artesanato"
	    }
	];
	
	$scope.adicionarSocio = function () {
		$scope.socios.push({id: $scope.socios.length+1});
	};
	
}]);