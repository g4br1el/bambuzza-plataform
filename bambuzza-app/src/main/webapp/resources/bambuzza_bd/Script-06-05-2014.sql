-- SQL Manager for PostgreSQL 5.1.1.1
-- ---------------------------------------
-- Host      : localhost
-- Database  : bambuzza_bd
-- Version   : PostgreSQL 9.0.2, compiled by Visual C++ build 1500, 64-bit



SET search_path = public, pg_catalog;
DROP SEQUENCE IF EXISTS public.tb_empresa_id_empresa_seq;
DROP SEQUENCE IF EXISTS public.id_questionario_seq;
DROP SEQUENCE IF EXISTS public.id_empresa_seq;
DROP TABLE IF EXISTS public.tb_opcao_resposta_pergunta;
DROP TABLE IF EXISTS public.tb_opcao_resposta;
DROP TABLE IF EXISTS public.tb_tipo_resposta;
DROP SEQUENCE IF EXISTS public.id_questionario_sobre_seq;
DROP SEQUENCE IF EXISTS public.id_questionario_empresa_seq;
DROP TABLE IF EXISTS public.tb_questionario_pergunta;
DROP TABLE IF EXISTS public.tb_questionario;
DROP TABLE IF EXISTS public.tb_resposta;
DROP TABLE IF EXISTS public.tb_telefone_tipo_usuario;
DROP TABLE IF EXISTS public.tb_tipo_telefone;
DROP TABLE IF EXISTS public.tb_telefone;
DROP TABLE IF EXISTS public.tb_tipo_usuario;
DROP TABLE IF EXISTS public.tb_pergunta;
DROP TABLE IF EXISTS public.tb_cidade;
DROP TABLE IF EXISTS public.tb_estado;
DROP TABLE IF EXISTS public.tb_pais;
DROP TABLE IF EXISTS public.tb_endereco;
DROP TABLE IF EXISTS public.tb_empresa;
DROP TABLE IF EXISTS public.tb_usuario;
SET check_function_bodies = false;
--
-- Structure for table tb_usuario (OID = 28106) :
--
CREATE TABLE public.tb_usuario (
    id_usuario integer DEFAULT nextval('id_usuario_seq'::regclass) NOT NULL,
    nome_usuario varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    foto varchar(250),
    cpf char(11),
    cargo varchar(80),
    descricao_profissional varchar(500),
    id_tipo_usuario smallint NOT NULL,
    id_empresa integer,
    dt_cadastro date NOT NULL,
    senha varchar(250) NOT NULL,
    id_endereco integer,
    id_questionario integer,
    sexo char(1)
) WITHOUT OIDS;
--
-- Structure for table tb_empresa (OID = 28121) :
--
CREATE TABLE public.tb_empresa (
    id_empresa integer DEFAULT nextval('id_empresa_seq'::regclass) NOT NULL,
    nome_empresa varchar(100) NOT NULL,
    cnpj char(14),
    aprovada boolean NOT NULL,
    img_logo varchar(250),
    pitch text,
    img_descricao varchar(250),
    plano_negocio varchar(250),
    dt_aprovacao date,
    id_endereco integer,
    id_questionario integer
) WITHOUT OIDS;
--
-- Structure for table tb_endereco (OID = 28133) :
--
CREATE TABLE public.tb_endereco (
    id_endereco integer DEFAULT nextval('id_endereco_seq'::regclass) NOT NULL,
    complemento varchar(250),
    cep char(8),
    id_pais integer NOT NULL,
    id_estado integer NOT NULL,
    id_cidade integer
) WITHOUT OIDS;
--
-- Structure for table tb_pais (OID = 28189) :
--
CREATE TABLE public.tb_pais (
    id_pais integer DEFAULT nextval('id_pais_seq'::regclass) NOT NULL,
    nome_pais varchar(100) NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_estado (OID = 28199) :
--
CREATE TABLE public.tb_estado (
    id_estado integer DEFAULT nextval('id_estado_seq'::regclass) NOT NULL,
    nome_estado varchar(100) NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_cidade (OID = 28209) :
--
CREATE TABLE public.tb_cidade (
    id_cidade integer DEFAULT nextval('id_cidade_seq'::regclass) NOT NULL,
    nome_cidade varchar(100) NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_pergunta (OID = 28265) :
--
CREATE TABLE public.tb_pergunta (
    id_pergunta integer DEFAULT nextval('id_pergunta_seq'::regclass) NOT NULL,
    texto_pergunta varchar(250) NOT NULL,
    exemplo_resposta text,
    id_tipo_usuario smallint NOT NULL,
    id_tipo_resposta integer NOT NULL,
    ativa boolean NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_tipo_usuario (OID = 28279) :
--
CREATE TABLE public.tb_tipo_usuario (
    id_tipo_usuario integer DEFAULT nextval('id_tipo_usuario_seq'::regclass) NOT NULL,
    nome varchar(50) NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_telefone (OID = 36329) :
--
CREATE TABLE public.tb_telefone (
    id_telefone integer DEFAULT nextval('id_telefone_seq'::regclass) NOT NULL,
    numero varchar(11) NOT NULL,
    tem_ramal boolean NOT NULL,
    id_tipo_numero integer NOT NULL,
    ramal varchar(4),
    id_usuario integer,
    id_empresa integer
) WITHOUT OIDS;
--
-- Structure for table tb_tipo_telefone (OID = 36337) :
--
CREATE TABLE public.tb_tipo_telefone (
    id_tipo_telefone integer DEFAULT nextval('id_tipo_telefone_seq'::regclass) NOT NULL,
    nome_tipo_telefone varchar(50) NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_telefone_tipo_usuario (OID = 36355) :
--
CREATE TABLE public.tb_telefone_tipo_usuario (
    id_tipo_telefone integer NOT NULL,
    id_tipo_usuario integer NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_resposta (OID = 36405) :
--
CREATE TABLE public.tb_resposta (
    id_resposta integer DEFAULT nextval('id_resposta_seq'::regclass) NOT NULL,
    resposta_texto_longo text,
    id_pergunta integer NOT NULL,
    id_questionario_empresa integer NOT NULL,
    resposta_boleana boolean,
    resposta_texto_curto varchar(100),
    id_resposta_multipla_escolha integer,
    resposta_numero integer
) WITHOUT OIDS;
--
-- Structure for table tb_questionario (OID = 36464) :
--
CREATE TABLE public.tb_questionario (
    id_questionario integer DEFAULT nextval('id_questionario_seq'::regclass) NOT NULL,
    enviado_selecao boolean NOT NULL,
    dt_enviado_selecao date,
    dt_ultima_modificacao timestamp(6) without time zone NOT NULL,
    id_usuario_ultima_modf integer NOT NULL,
    id_tipo_usuario integer NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_questionario_pergunta (OID = 36531) :
--
CREATE TABLE public.tb_questionario_pergunta (
    id_questionario integer NOT NULL,
    id_pergunta integer NOT NULL
) WITHOUT OIDS;
--
-- Definition for sequence id_questionario_empresa_seq (OID = 36546) :
--
CREATE SEQUENCE public.id_questionario_empresa_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence id_questionario_sobre_seq (OID = 36623) :
--
CREATE SEQUENCE public.id_questionario_sobre_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;
--
-- Structure for table tb_tipo_resposta (OID = 36711) :
--
CREATE TABLE public.tb_tipo_resposta (
    id_tipo_resposta integer DEFAULT nextval('id_tipo_resposta_seq'::regclass) NOT NULL,
    tipo_nome varchar(25) NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_opcao_resposta (OID = 36726) :
--
CREATE TABLE public.tb_opcao_resposta (
    id_opcao_resposta integer DEFAULT nextval('id_opcao_resposta_seq'::regclass) NOT NULL,
    opcao_nome varchar(250) NOT NULL,
    opcao_ativa boolean NOT NULL,
    id_pergunta integer NOT NULL
) WITHOUT OIDS;
--
-- Structure for table tb_opcao_resposta_pergunta (OID = 36739) :
--
CREATE TABLE public.tb_opcao_resposta_pergunta (
    id_opcao_resposta integer NOT NULL,
    id_resposta integer NOT NULL
) WITHOUT OIDS;
--
-- Definition for sequence id_empresa_seq (OID = 36844) :
--
CREATE SEQUENCE public.id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence id_questionario_seq (OID = 36923) :
--
CREATE SEQUENCE public.id_questionario_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence tb_empresa_id_empresa_seq (OID = 37087) :
--
CREATE SEQUENCE public.tb_empresa_id_empresa_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;
--
-- Definition for index usuario_pkey (OID = 28113) :
--
ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT usuario_pkey
    PRIMARY KEY (id_usuario);
--
-- Definition for index usuario_email_key (OID = 28115) :
--
ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT usuario_email_key
    UNIQUE (email);
--
-- Definition for index empresa_nome_key (OID = 28127) :
--
ALTER TABLE ONLY tb_empresa
    ADD CONSTRAINT empresa_nome_key
    UNIQUE (nome_empresa);
--
-- Definition for index empresa_cnpj_key (OID = 28129) :
--
ALTER TABLE ONLY tb_empresa
    ADD CONSTRAINT empresa_cnpj_key
    UNIQUE (cnpj);
--
-- Definition for index endereco_pkey (OID = 28137) :
--
ALTER TABLE ONLY tb_endereco
    ADD CONSTRAINT endereco_pkey
    PRIMARY KEY (id_endereco);
--
-- Definition for index usuario_cpf_key (OID = 28157) :
--
ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT usuario_cpf_key
    UNIQUE (cpf);
--
-- Definition for index pais_pkey (OID = 28193) :
--
ALTER TABLE ONLY tb_pais
    ADD CONSTRAINT pais_pkey
    PRIMARY KEY (id_pais);
--
-- Definition for index pais_nome_key (OID = 28195) :
--
ALTER TABLE ONLY tb_pais
    ADD CONSTRAINT pais_nome_key
    UNIQUE (nome_pais);
--
-- Definition for index estado_pkey (OID = 28203) :
--
ALTER TABLE ONLY tb_estado
    ADD CONSTRAINT estado_pkey
    PRIMARY KEY (id_estado);
--
-- Definition for index estado_nome_key (OID = 28205) :
--
ALTER TABLE ONLY tb_estado
    ADD CONSTRAINT estado_nome_key
    UNIQUE (nome_estado);
--
-- Definition for index cidade_pkey (OID = 28213) :
--
ALTER TABLE ONLY tb_cidade
    ADD CONSTRAINT cidade_pkey
    PRIMARY KEY (id_cidade);
--
-- Definition for index cidade_nome_key (OID = 28215) :
--
ALTER TABLE ONLY tb_cidade
    ADD CONSTRAINT cidade_nome_key
    UNIQUE (nome_cidade);
--
-- Definition for index endereco_fk (OID = 28222) :
--
ALTER TABLE ONLY tb_endereco
    ADD CONSTRAINT endereco_fk
    FOREIGN KEY (id_pais) REFERENCES tb_pais(id_pais) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index endereco_fk1 (OID = 28227) :
--
ALTER TABLE ONLY tb_endereco
    ADD CONSTRAINT endereco_fk1
    FOREIGN KEY (id_estado) REFERENCES tb_estado(id_estado) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index endereco_fk2 (OID = 28232) :
--
ALTER TABLE ONLY tb_endereco
    ADD CONSTRAINT endereco_fk2
    FOREIGN KEY (id_cidade) REFERENCES tb_cidade(id_cidade) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index pergunta_pkey (OID = 28272) :
--
ALTER TABLE ONLY tb_pergunta
    ADD CONSTRAINT pergunta_pkey
    PRIMARY KEY (id_pergunta);
--
-- Definition for index pergunta_texto_pergunta_key (OID = 28274) :
--
ALTER TABLE ONLY tb_pergunta
    ADD CONSTRAINT pergunta_texto_pergunta_key
    UNIQUE (texto_pergunta);
--
-- Definition for index tipo_usuario_pkey (OID = 28283) :
--
ALTER TABLE ONLY tb_tipo_usuario
    ADD CONSTRAINT tipo_usuario_pkey
    PRIMARY KEY (id_tipo_usuario);
--
-- Definition for index tipo_usuario_nome_key (OID = 28285) :
--
ALTER TABLE ONLY tb_tipo_usuario
    ADD CONSTRAINT tipo_usuario_nome_key
    UNIQUE (nome);
--
-- Definition for index telefone_pkey (OID = 36333) :
--
ALTER TABLE ONLY tb_telefone
    ADD CONSTRAINT telefone_pkey
    PRIMARY KEY (id_telefone);
--
-- Definition for index tipo_telefone_pkey (OID = 36341) :
--
ALTER TABLE ONLY tb_tipo_telefone
    ADD CONSTRAINT tipo_telefone_pkey
    PRIMARY KEY (id_tipo_telefone);
--
-- Definition for index telefone_fk (OID = 36343) :
--
ALTER TABLE ONLY tb_telefone
    ADD CONSTRAINT telefone_fk
    FOREIGN KEY (id_tipo_numero) REFERENCES tb_tipo_telefone(id_tipo_telefone) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index telefone_tipo_usuario_pkey (OID = 36358) :
--
ALTER TABLE ONLY tb_telefone_tipo_usuario
    ADD CONSTRAINT telefone_tipo_usuario_pkey
    PRIMARY KEY (id_tipo_telefone, id_tipo_usuario);
--
-- Definition for index usuario_fk1 (OID = 36379) :
--
ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT usuario_fk1
    FOREIGN KEY (id_tipo_usuario) REFERENCES tb_tipo_usuario(id_tipo_usuario) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index pergunta_fk (OID = 36398) :
--
ALTER TABLE ONLY tb_pergunta
    ADD CONSTRAINT pergunta_fk
    FOREIGN KEY (id_tipo_usuario) REFERENCES tb_tipo_usuario(id_tipo_usuario) ON UPDATE CASCADE ON DELETE CASCADE;
--
-- Definition for index resposta_pkey (OID = 36412) :
--
ALTER TABLE ONLY tb_resposta
    ADD CONSTRAINT resposta_pkey
    PRIMARY KEY (id_resposta);
--
-- Definition for index resposta_fk (OID = 36414) :
--
ALTER TABLE ONLY tb_resposta
    ADD CONSTRAINT resposta_fk
    FOREIGN KEY (id_pergunta) REFERENCES tb_pergunta(id_pergunta) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index questionario_empresa_pergunta_pkey (OID = 36534) :
--
ALTER TABLE ONLY tb_questionario_pergunta
    ADD CONSTRAINT questionario_empresa_pergunta_pkey
    PRIMARY KEY (id_questionario, id_pergunta);
--
-- Definition for index telefone_tipo_usuario_fk (OID = 36570) :
--
ALTER TABLE ONLY tb_telefone_tipo_usuario
    ADD CONSTRAINT telefone_tipo_usuario_fk
    FOREIGN KEY (id_tipo_telefone) REFERENCES tb_tipo_telefone(id_tipo_telefone) ON UPDATE CASCADE;
--
-- Definition for index telefone_tipo_usuario_fk1 (OID = 36575) :
--
ALTER TABLE ONLY tb_telefone_tipo_usuario
    ADD CONSTRAINT telefone_tipo_usuario_fk1
    FOREIGN KEY (id_tipo_usuario) REFERENCES tb_tipo_usuario(id_tipo_usuario) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index tb_telefone_fk (OID = 36585) :
--
ALTER TABLE ONLY tb_telefone
    ADD CONSTRAINT tb_telefone_fk
    FOREIGN KEY (id_usuario) REFERENCES tb_usuario(id_usuario) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index tb_usuario_id_endereco_key (OID = 36597) :
--
ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT tb_usuario_id_endereco_key
    UNIQUE (id_endereco);
--
-- Definition for index tb_usuario_fk (OID = 36599) :
--
ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT tb_usuario_fk
    FOREIGN KEY (id_endereco) REFERENCES tb_endereco(id_endereco) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index tb_empresa_id_endereco_key (OID = 36604) :
--
ALTER TABLE ONLY tb_empresa
    ADD CONSTRAINT tb_empresa_id_endereco_key
    UNIQUE (id_endereco);
--
-- Definition for index tb_empresa_fk (OID = 36606) :
--
ALTER TABLE ONLY tb_empresa
    ADD CONSTRAINT tb_empresa_fk
    FOREIGN KEY (id_endereco) REFERENCES tb_endereco(id_endereco) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index tb_usuario_id_questionario_key (OID = 36652) :
--
ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT tb_usuario_id_questionario_key
    UNIQUE (id_questionario);
--
-- Definition for index tb_questionario_fk (OID = 36699) :
--
ALTER TABLE ONLY tb_questionario
    ADD CONSTRAINT tb_questionario_fk
    FOREIGN KEY (id_tipo_usuario) REFERENCES tb_tipo_usuario(id_tipo_usuario) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index q_empresa_pergunta_fk1 (OID = 36704) :
--
ALTER TABLE ONLY tb_questionario_pergunta
    ADD CONSTRAINT q_empresa_pergunta_fk1
    FOREIGN KEY (id_pergunta) REFERENCES tb_pergunta(id_pergunta) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index tb_tipo_resposta_pkey (OID = 36715) :
--
ALTER TABLE ONLY tb_tipo_resposta
    ADD CONSTRAINT tb_tipo_resposta_pkey
    PRIMARY KEY (id_tipo_resposta);
--
-- Definition for index tb_tipo_resposta_tipo_nome_key (OID = 36717) :
--
ALTER TABLE ONLY tb_tipo_resposta
    ADD CONSTRAINT tb_tipo_resposta_tipo_nome_key
    UNIQUE (tipo_nome);
--
-- Definition for index tb_pergunta_fk (OID = 36719) :
--
ALTER TABLE ONLY tb_pergunta
    ADD CONSTRAINT tb_pergunta_fk
    FOREIGN KEY (id_tipo_resposta) REFERENCES tb_tipo_resposta(id_tipo_resposta) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index tb_opcao_resposta_pkey (OID = 36730) :
--
ALTER TABLE ONLY tb_opcao_resposta
    ADD CONSTRAINT tb_opcao_resposta_pkey
    PRIMARY KEY (id_opcao_resposta);
--
-- Definition for index tb_opcao_resposta_opcao_resposta_nome_key (OID = 36732) :
--
ALTER TABLE ONLY tb_opcao_resposta
    ADD CONSTRAINT tb_opcao_resposta_opcao_resposta_nome_key
    UNIQUE (opcao_nome);
--
-- Definition for index tb_opcao_resposta_pergunta_pkey (OID = 36742) :
--
ALTER TABLE ONLY tb_opcao_resposta_pergunta
    ADD CONSTRAINT tb_opcao_resposta_pergunta_pkey
    PRIMARY KEY (id_opcao_resposta, id_resposta);
--
-- Definition for index tb_opcao_resposta_fk (OID = 36754) :
--
ALTER TABLE ONLY tb_opcao_resposta
    ADD CONSTRAINT tb_opcao_resposta_fk
    FOREIGN KEY (id_pergunta) REFERENCES tb_pergunta(id_pergunta) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index tb_opcao_resposta_pergunta_fk1 (OID = 36764) :
--
ALTER TABLE ONLY tb_opcao_resposta_pergunta
    ADD CONSTRAINT tb_opcao_resposta_pergunta_fk1
    FOREIGN KEY (id_resposta) REFERENCES tb_resposta(id_resposta) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index tb_opcao_resposta_pergunta_fk (OID = 36769) :
--
ALTER TABLE ONLY tb_opcao_resposta_pergunta
    ADD CONSTRAINT tb_opcao_resposta_pergunta_fk
    FOREIGN KEY (id_opcao_resposta) REFERENCES tb_opcao_resposta(id_opcao_resposta);
--
-- Definition for index questionario_inscricao_pkey (OID = 36925) :
--
ALTER TABLE ONLY tb_questionario
    ADD CONSTRAINT questionario_inscricao_pkey
    PRIMARY KEY (id_questionario);
--
-- Definition for index resposta_fk1 (OID = 36927) :
--
ALTER TABLE ONLY tb_resposta
    ADD CONSTRAINT resposta_fk1
    FOREIGN KEY (id_questionario_empresa) REFERENCES tb_questionario(id_questionario) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index tb_usuario_fk1 (OID = 36932) :
--
ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT tb_usuario_fk1
    FOREIGN KEY (id_questionario) REFERENCES tb_questionario(id_questionario) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index tb_q_empresa_pergunta_fk (OID = 36937) :
--
ALTER TABLE ONLY tb_questionario_pergunta
    ADD CONSTRAINT tb_q_empresa_pergunta_fk
    FOREIGN KEY (id_questionario) REFERENCES tb_questionario(id_questionario) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index tb_empresa_id_questionario_key (OID = 37075) :
--
ALTER TABLE ONLY tb_empresa
    ADD CONSTRAINT tb_empresa_id_questionario_key
    UNIQUE (id_questionario);
--
-- Definition for index tb_empresa_fk1 (OID = 37082) :
--
ALTER TABLE ONLY tb_empresa
    ADD CONSTRAINT tb_empresa_fk1
    FOREIGN KEY (id_questionario) REFERENCES tb_questionario(id_questionario) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index empresa_pkey (OID = 37089) :
--
ALTER TABLE ONLY tb_empresa
    ADD CONSTRAINT empresa_pkey
    PRIMARY KEY (id_empresa);
--
-- Definition for index tb_telefone_fk1 (OID = 37091) :
--
ALTER TABLE ONLY tb_telefone
    ADD CONSTRAINT tb_telefone_fk1
    FOREIGN KEY (id_empresa) REFERENCES tb_empresa(id_empresa) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index usuario_fk (OID = 37096) :
--
ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT usuario_fk
    FOREIGN KEY (id_empresa) REFERENCES tb_empresa(id_empresa) ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE;
--
-- Comments
--
COMMENT ON SCHEMA public IS 'standard public schema';
